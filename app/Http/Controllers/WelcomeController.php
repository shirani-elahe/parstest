<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function index(){
        $tags = Video::allTags();;
        $videos = Video::paginate(5);
        return view('welcome',compact('videos','tags'));
    }
}
