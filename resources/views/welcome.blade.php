@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <h3 class="mb-0">
                            <span class="text-dark" >Tags :</span>
                        </h3>
                        @foreach($tags as $tag)
                            <span class="card-text mb-auto" style="margin: 10px">
                                <a class="text-dark" href="/video/tag/{{$tag}}">{{$tag}}</a>
                            </span>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                @if(!$videos->isEmpty())
                    @foreach($videos as $video)
                        <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                            <div class="card-body d-flex flex-column align-items-start">
                                <strong class="d-inline-block mb-2 text-primary">Tags : {{$video->tagList}}</strong>
                                <h3 class="mb-0">
                                    <a class="text-dark" href="{{route('video.show',['id'=>$video->id])}}">{{$video->title}}</a>
                                </h3>
                                <p class="card-text mb-auto">For See Video Click View Detail ...</p>
                                <div class="mb-1 text-muted">View Count : {{$video->view_count}}</div>
                                <a href="{{route('video.show',['id'=>$video->id])}}">View details »</a>
                            </div>
                            <img class="card-img-right flex-auto d-none d-lg-block" data-src="holder.js/200x250?theme=thumb"  style="width: 300px; height: 200px;" src="/images/video.jpg" data-holder-rendered="true">
                        </div>
                        @if(!$loop->last)
                            <hr>
                        @endif
                    @endforeach
                    {!! $videos->render() !!}
                @else
                    <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                        <div class="card-body d-flex flex-column align-items-start">
                            <h4 class="mb-0">
                                <a class="text-dark" href="">Not Found Any Video With this Tag.</a>
                            </h4>
                        </div>
                        <img class="card-img-right flex-auto d-none d-lg-block" data-src="holder.js/200x250?theme=thumb"  style="width: 300px; height: 200px;" src="/images/video.jpg" data-holder-rendered="true">
                    </div>
                @endif



            </div>
        </div>
        <hr>
        <footer class="container">
            <p>© Company 2017-2018</p>
        </footer>
    </div>
@endsection

