@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="card flex-md-row mb-4 shadow-sm h-md-250" style="margin: 0 auto">
                <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-primary">{{$video->tagList}}</strong>
                    <h3 class="mb-0">
                        <a class="text-dark" href="#">{{$video->title}}</a>
                    </h3>
                    <div class="mb-1 text-muted" style="margin: 20px"><span style="font-weight: bold">View Count :</span> {{$video->view_count}}</div>
                    <p class="mb-1 text-muted" style="margin: 20px"><span style="font-weight: bold">Video Created at :</span>  {{date('F d,Y',strtotime($video->created_at))}} at {{date('g:ia',strtotime($video->created_at))}}</p>
                </div>

                <video  controls style="margin: 10px" autoplay width="500" height="400">
                    <source src="{{'/'.$video->source}}" type="video/mp4">
                </video>
                <br>
            </div>
        </div>
    </div>
@endsection